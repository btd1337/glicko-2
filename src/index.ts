import Match from './match.entity';
import Player from './player.entity';
import { Utils } from './utils';
import { Winner } from './winner.enum';

import Chance from 'chance';

// Player Params
export const MAX_SKILL: number = 3000;
export const DEFAULT_RATING: number = MAX_SKILL / 2;
const DEFAULT_RD: number = 150;
const DEFAULT_VOLATILITY: number = 0.4;
const DEFAULT_TAU: number = 1.2;

// Instance
const NUM_STUDENTS: number = 500;
const NUM_STUDENTS_LENGTH: number = NUM_STUDENTS.toString().length;
const NUM_QUESTIONS: number = 50;
const NUM_QUESTIONS_LENGTH: number = NUM_QUESTIONS.toString().length;

// Settings
export const RATING_LENGTH: number = DEFAULT_RATING.toString().length + 3; // value + '.XX'
export const RD_LENGTH: number = DEFAULT_RD.toString().length + 3;
export const UNCERTAINTY_RATE: number = 0.1;
export const DEFAULT_PLAYER_DEVIATION = 300;
export const SEED = 10;

export const RANDOM = new Chance(SEED);

function createPlayer(name: string, skill: number): Player {
	return Player.createPlayerFactory(name, skill, {
		defaultRating: DEFAULT_RATING,
		defaultRatingDeviation: DEFAULT_RD,
		defaultVolatility: DEFAULT_VOLATILITY,
		tau: DEFAULT_TAU,
	});
}

function recordMatch(match: Match) {
	let teamASumRatings =
		match.aTeam.length == 1 ? match.aTeam[0].skill : Utils.getTeamRatingsSum(match.aTeam);
	let teamBSumRatings =
		match.bTeam.length == 1 ? match.bTeam[0].skill : Utils.getTeamRatingsSum(match.bTeam);

	const numberDraw = Utils.randomInt(0, teamASumRatings + teamBSumRatings);
	const winner: Winner = numberDraw < teamASumRatings ? Winner.PLAYER_A : Winner.PLAYER_B;

	switch (winner) {
		case Winner.PLAYER_A: {
			match.reportTeamAWon();
			break;
		}
		case Winner.PLAYER_B: {
			match.reportTeamBWon();
			break;
		}
		default: {
			console.log('Invalid Result');
		}
	}
}

function main() {
	let students: Player[] = new Array<Player>();
	let questions: Player[] = new Array<Player>();

	for (let i: number = 1; i <= NUM_STUDENTS; i++) {
		let id: string = Utils.normalizeStringLength(i.toString(), NUM_STUDENTS_LENGTH);
		students.push(createPlayer(`Student ${id}`, Utils.random_normal()));
	}

	for (let i: number = 1; i <= NUM_QUESTIONS; i++) {
		let id: string = Utils.normalizeStringLength(i.toString(), NUM_QUESTIONS_LENGTH);
		questions.push(createPlayer(`Question ${id}`, Utils.random_normal()));
	}

	let question;
	let student;

	// Todos os estudantes realizam X questões

	/*
	for (let i = 0; i < students.length; i++) {
		for (let j = 0; j < 20; j++) {
			question = Utils.randomInt(0, NUM_QUESTIONS - 1);
			recordMatch(new Match(students[i], questions[question]));
			students[i].updateRating();
			questions[question].updateRating();
		}
	}
	*/

	// Todas as questões são resolvidas até chegarem em uma margem de erro específica

	for (let i = 0; i < questions.length; i++) {
		do {
			student = Utils.randomInt(0, NUM_STUDENTS - 1);
			recordMatch(new Match(students[student], questions[i]));
			questions[i].updateRating();
			students[student].updateRating();
			console.log(`${questions[i].toStringFormatted(RATING_LENGTH, RD_LENGTH)}`);
		} while (!questions[i].checkSkillConfiability(UNCERTAINTY_RATE));
	}

	/* 
	console.log('\n\n--- ESTUDANTES ---\n\n');
	Utils.print_players(students);
	*/

	console.log('\n\n--- QUESTÕES ---\n\n');
	Utils.print_players(questions);
}

main();
