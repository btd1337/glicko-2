import {
	DEFAULT_PLAYER_DEVIATION,
	DEFAULT_RATING,
	RANDOM,
	RATING_LENGTH,
	RD_LENGTH,
} from './index';
import Player from './player.entity';

export class Utils {
	public static randomInt = (min: number, max: number): number => {
		return Math.floor(Math.random() * (max - min + 1) + min);
	};

	public static random_normal(
		mean: number = DEFAULT_RATING,
		dev: number = DEFAULT_PLAYER_DEVIATION,
	): number {
		return RANDOM.normal({ mean: mean, dev: dev });
	}

	public static getTeamRatingsSum = (team: Player[]): number => {
		let teamRatingsSum = 0;

		for (let i = 0; i < team.length; i++) {
			teamRatingsSum += team[i].skill;
		}
		return teamRatingsSum;
	};

	public static check_normal_distribution(
		scale: number = DEFAULT_RATING / 10,
		granularity: number = 20,
		numElements: number = 1000000,
	): void {
		let values: number[] = new Array(granularity);
		values.fill(0);

		for (let i: number = 0; i < numElements; i++) {
			let index: number = Number((Utils.random_normal() / scale).toFixed());
			if (index >= 0 && index < granularity) {
				values[index] += 1;
			}
		}

		for (let i = 0; i < granularity; i++) {
			console.log(`${i * scale} - ${(i + 1) * scale}: ${values[i]}`);
		}
	}

	public static print_players(players: Player[]): void {
		let maxMatches: number = 0;
		let mediumRD: number = 0;
		let mediumMatches: number = 0;
		for (let i: number = 0; i < players.length; i++) {
			if (players[i].numMatches > maxMatches) {
				maxMatches = players[i].numMatches;
			}
			mediumRD += players[i].ratingDeviation;
			mediumMatches += players[i].numMatches;
			console.log(players[i].toStringFormatted(RATING_LENGTH, RD_LENGTH));
		}
		mediumRD = mediumRD / players.length;

		// Remove worst case
		mediumMatches = mediumMatches - maxMatches;
		mediumMatches = mediumMatches / (players.length - 1);
		console.log(
			`\nMax Matches: ${maxMatches} | Medium Matches: ${mediumMatches.toFixed(
				2,
			)} | Medium RD: ${mediumRD.toFixed(2)}`,
		);
	}

	public static normalizeStringLength(value: string, length: number): string {
		let formatedValue: string = value;
		let diffLenght = length - formatedValue.length;

		if (diffLenght != 0) {
			formatedValue = formatedValue.padStart(length, '0');
		}
		return formatedValue;
	}
}
