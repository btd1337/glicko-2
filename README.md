# Sistema de Classificação Glicko 2

Todo jogador no sistema Glicko 2 tem as seguintes propriedades: **classificação** (rating), r, **desvio de classificação** (rating deviation), RD, e **volatilidade da classificação**, σ.

## Volatilidade

Significado de **volatidade**: Qualidade do que sofre constantes mudanças; característica do que é volátil, do que não é firme, daquilo que muda constantemente ou se vaporiza.

A medida de volatilidade:

-   Indica o grau de flutuação esperada na classificação de um jogador.

-   É alta quando o jogador tem performances erraticas (por exemplo, quando o jogador teve resultados excepcionalmente variantes depois de um período de estabilidade)
-   É baixa quando o jogador tem uma performance consistente.

## Força do Jogador

Geralmente é informativo resumir a força de um jogador na forma de um intervalo, ao invez de simplesmente relatar uma classificação.

Uma forma de fazer isso é relatar um intervalo de 95% de confiança.

-   O menor valor do intervalo é a classificação do jogador menos duas vezes o RD.
-   O maior valor é a classificação do jogador mais duas vezes o RD.

### Exemplo

Se a **classificação** de um jogador é **1850** e o **RD** é **50**, o **intervalo de força** iria de **1750 até 1950**.

---

Quando um jogador tem RD baixo, o intervalo seria estreito, de modo que teriamos 95% de certeza sobre a força do jogador estar em um pequeno intervalo de valores.

> Obs: A medida de volatilidade não aparece no cálculo deste intervalo

## Tau

O parâmetro **tau**, que controla a mudança na volatilidade do jogador ao longo do tempo. Valores menores evitam que as medidas de volatilidade mudem em grandes quantidades. Deve ser um único número. Mark Glickman sugere um valor entre 0,3 e 1,2. Um valor não positivo pode ser especificado, caso em que as volatilidades nunca são atualizadas.

Ele é um valor global que controla a quantidade de energia que a Sigma tem. Jogos com alta variabilidade aleatória precisam de um **tau** baixo em torno de 0,2 para contabilizar a aleatoriedade nos jogos. Jogos com menor variabilidade podem ser executados com valores **tau** mais altos. Talvez até 1,2 no máximo. Em tipos de jogos com altos scores **tau**, há muito pouco comportamento aleatório. Portanto, uma vitória ou uma derrota é mais decisiva e a Sigma deve se ajustar com um fator maior.

## As Fórmulas

Para aplicar o algoritmo de classificação, nós tratamos uma coleção de jogos dentro de um _período de classificação_ que ocorrem simultâneamente.

Os jogadores devem ter classificações, RDs e volatidades no início do período de classificação, os resultados das partidas seriam observados e, em seguida, as classificações, RDs e volatilidades atualizadas são calculadas no final do _período de classificação_ (que seria então usado como informação pré-período para o _período de classificação_ subsequente).

### Períodos de Classificação

O sistema Glicko 2 funciona melhor quando o número de partidas em um período é de moderado para grande.

-   Uma boa média é de pelo menos 10 a 15 partidas por jogador em um _período de classificação_.

> A duração de um _período de classificação_ fica a critério do administrador

## Como Executar

1. Instalar as dependências

```sh
npm install
```

2. Rodar

```sh
npm start
```
